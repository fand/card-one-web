# demo

## 安装 pnpm

```
npm install pnpm -g
```

## 安装项目运行环境

```
pnpm i
```

## 启动项目 page-*-react：http://127.0.0.1:8080/

```
pnpm -F @cardone-web/page-* dev
```

## 打包到生产环境

```
pnpm -F @cardone-web/page-* build:prod
```



