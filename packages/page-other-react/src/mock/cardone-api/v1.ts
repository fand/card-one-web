import { MockMethod } from "vite-plugin-mock";

import Mock from "mockjs";

export default [
  {
    url: "/mock/cardone-api/v1/logind",
    method: "post",
    timeout: 1000,
    response: () => {
      return {
        data: {
          authorization: "test",
          authorities: ["ROLE_test", "ROLE_administrator", "navigation:view:/list", "navigation:view:/downloadPack"],
          loginUser: {
            userId: "administrator",
            username: "admin",
            name: "超级管理员",
          },
        },
      };
    },
  },
  {
    url: `/mock/cardone-api/v1/other`,
    method: "post",
    timeout: 1000,
    response: () => {
      return {
        data: {
          navigationList: [
            {
              id: "/module-1",
              code: "/module-1",
              href: "/module-1",
              name: "模块-1",
            },
            {
              id: "/module-2",
              code: "/module-2",
              href: "/module-2",
              name: "模块-2",
            },
            {
              id: "/module-3",
              code: "/module-3",
              href: "/module-3",
              name: "模块-3",
            },
            {
              id: "/module-4",
              code: "/module-4",
              href: "/module-4",
              name: "模块-4",
            },
            {
              id: "/module-5",
              code: "/module-5",
              href: "/module-5",
              name: "模块-5",
            },
          ],
        },
      };
    },
  },
] as MockMethod[];
