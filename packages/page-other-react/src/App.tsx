import { Layout, Space, Breadcrumb, Menu, Spin, message } from 'antd';
import * as v1Api from '~/api/v1';
import { axios1, withErrorCustom, withErrorHelper, withExtraParams } from '@cardone-web/core/axios';
import LoadingWaitFor from '~/components/LoadingWaitFor';
import { routes } from './router';

const navigationListToMenuItems = (navigationList: any[]) => {
  if (!navigationList) {
    return [];
  }

  return navigationList.map(it => {
    let item = {
      label: it.name,
      key: it.href,
    } as any;

    if (it.href) {
      item.href = it.href;
    }

    if (it.childs) {
      item.children = navigationListToMenuItems(it.childs);
    }

    return item;
  });
};

const findRouteListByRouteListAndPath: any = (routeList: any[], parentPath = '', path: String) => {
  for (let route of routeList) {
    if (path === route.path) {
      return [route];
    }

    if (route.children) {
      let items = findRouteListByRouteListAndPath(route.children, parentPath + route.path, path);

      if (items.length > 0) {
        return [route, ...items];
      }
    }
  }

  return [];
};

const tokenKey = `${import.meta.env.VITE_PROJECT_COOKIE_PREFIX}authorization`;

function App() {
  //#region 定义状态变量
  const [securityAuthorization, setSecurityAuthorization] = useCookieState(tokenKey);

  const setSecurityAuthorities = useSetAtom(securityAuthoritiesAtom);

  const setSecurityLoginUser = useSetAtom(securityLoginUserAtom);
  //#endregion

  //#region axios1 config
  useEffect(() => {
    withExtraParams(
      axios1,
      () => {
        let params = {} as any;

        params[tokenKey] = securityAuthorization;

        return params;
      },
      'headers'
    );

    withErrorCustom(axios1, response => {
      return response.status === 200 && !response.data?.errorCode;
    });

    withErrorHelper(axios1, (error: any) => {
      if (!['login repeat', 'login expiration', 'login failure'].includes(error.response?.data?.errorCode)) {
        message.error({
          content: `${error.config?.errorMessage ?? ''}${error.response?.data?.error ?? error.message}`,
          duration: 5,
        });

        return;
      }

      message.error({
        content: `${error.config?.errorMessage ?? ''}${error.response?.data?.error ?? '登录失效：请重新登录'}`,
        duration: error.data?.errorCode === 'login failure' ? 15 : 10,
        onClose: () => {
          window.location.href = error.config.loginUrl ?? '/login.html';
        },
      });
    });
  }, []);
  //#endregion

  //#region 定时从服务器获取:token、权限、当前登录用户信息
  useRequest<any, any[]>(() => v1Api.logind({}), {
    pollingInterval: 30000,
    onSuccess: result => {
      if (result?.data?.data?.authorization) {
        setSecurityAuthorization(result?.data?.data?.authorization ?? '');
      }

      setSecurityAuthorities(result?.data?.data?.authorities ?? []);
      setSecurityLoginUser(result?.data?.data?.loginUser ?? {});
    },
  });
  //#endregion

  const navigate = useNavigate();

  const location = useLocation();

  const { data: indexData, loading: indexLoading } = useRequest<any, any[]>(() => v1Api.other({}));

  const memuItems = useCreation(() => {
    if (!indexData?.data?.data?.navigationList) {
      return [];
    }

    return navigationListToMenuItems(indexData?.data?.data?.navigationList);
  }, [indexData]);

  const breadcrumbs: any[] = useCreation(() => {
    return findRouteListByRouteListAndPath(routes, '', location.pathname);
  }, [location.pathname]);

  const memuDefaultOpenKeys: any[] = useCreation(() => {
    return breadcrumbs.map(it => it.path);
  }, [breadcrumbs]);

  const memuDefaultSelectedKeys = useCreation(() => {
    return [location.pathname];
  }, [location.pathname]);

  const handleClickMenu = useCreation(
    () => (e: any) => {
      if (e.key) {
        navigate(e.key);
      }
    },
    [indexData?.data?.data?.navigationList]
  );

  if (!securityAuthorization) {
    return <LoadingWaitFor />;
  }

  return (
    <Layout id="app">
      <Layout>
        <Layout.Sider collapsible>
          {indexLoading ? (
            <LoadingWaitFor />
          ) : (
            <Menu
              mode="inline"
              theme="dark"
              defaultSelectedKeys={memuDefaultSelectedKeys}
              defaultOpenKeys={memuDefaultOpenKeys}
              items={memuItems}
              onClick={handleClickMenu}
            />
          )}
        </Layout.Sider>
        <Layout>
          <Space size="small">
            <Breadcrumb>
              <Breadcrumb.Item>业务应用-x</Breadcrumb.Item>
              {breadcrumbs.length > 0 ? breadcrumbs.map(it => <Breadcrumb.Item key={it.path}>{it.name ?? it.path}</Breadcrumb.Item>) : null}
            </Breadcrumb>
          </Space>
          <Layout.Content>
            <Outlet />
          </Layout.Content>
        </Layout>
      </Layout>
    </Layout>
  );
}

export default App;
