import { Spin } from 'antd';

const LoadingWaitFor = () => {
  return (
    <div className="loading-wait-for">
      <Spin size="large" tip="载入中..." />
    </div>
  );
};

export default LoadingWaitFor;
