import { HashRouter } from 'react-router-dom';
import { Suspense } from 'react';
import LoadingWaitFor from '~/components/LoadingWaitFor';

const LazyElement = ({ lazyComponent: LazyComponent }: any) => {
  return (
    <Suspense fallback={<LoadingWaitFor />}>
      <LazyComponent />
    </Suspense>
  );
};

export const routes = [
  {
    path: '/',
    element: <LazyElement lazyComponent={lazy(() => import('~/App'))} />,
    children: [
      {
        index: true,
        element: <Navigate to="/usercenter/department" />,
      },
      {
        path: 'index',
        element: <LazyElement lazyComponent={lazy(() => import('~/components/index/Index'))} />,
      },
      {
        path: 'test',
        element: <LazyElement lazyComponent={lazy(() => import('~/components/test/Index'))} />,
      },
      {
        path: 'demo',
        element: <LazyElement lazyComponent={lazy(() => import('~/components/demo/Index'))} />,
      },
      {
        path: '*',
        element: <LazyElement lazyComponent={lazy(() => import('~/components/NotFound'))} />,
      },
    ],
  },
  {
    path: '/redirect',
    element: <LazyElement lazyComponent={lazy(() => import('~/components/Redirect'))} />,
  },
  {
    path: '*',
    element: <LazyElement lazyComponent={lazy(() => import('~/components/NotFound'))} />,
  },
];

//根据路径获取路由
const checkAuth = (routers: any, path: String) => {
  for (const data of routers) {
    if (data.path === path) {
      return data;
    }

    if (data.children) {
      const res: any = checkAuth(data.children, path);

      if (res) {
        return res;
      }
    }
  }

  return null;
};

const Routers = () => useRoutes(routes);

export default () => {
  return (
    <HashRouter>
      <Routers />
    </HashRouter>
  );
};
