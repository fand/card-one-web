import { MockMethod } from "vite-plugin-mock";

export default [
  {
    url: "/mock/cardone-api/v1/configuration/dictionary/listByDictTypeCode",
    method: "post",
    timeout: 1000,
    response: ({ body }) => {
      if ("tool-detection-risk-level" === body.dictTypeCode) {
        return {
          data: [
            {
              value: "高风险",
              label: "高风险",
            },
            {
              value: "中风险",
              label: "中风险",
            },
            {
              value: "低风险",
              label: "低风险",
            },
          ],
        };
      }

      if ("tool-detection-type" === body.dictTypeCode) {
        return {
          data: [
            {
              value: "代理转发",
              label: "代理转发",
            },
            {
              value: "密码抓取",
              label: "密码抓取",
            },
            {
              value: "内网扫描",
              label: "内网扫描",
            },
          ],
        };
      }

      if ("handle-type" === body.dictTypeCode) {
        return {
          data: [
            {
              value: "wait for",
              label: "未处理",
            },
            {
              value: "processed",
              label: "已处理",
            },
          ],
        };
      }

      return {
        data: [],
      };
    },
  },
] as MockMethod[];
