import { MockMethod } from "vite-plugin-mock";

import Mock from "mockjs";

export default [
  {
    url: "/mock/cardone-api/v1/logind",
    method: "post",
    timeout: 1000,
    response: () => {
      return {
        data: {
          authorization: "test",
          authorities: ["ROLE_test", "ROLE_administrator", "navigation:view:/list", "navigation:view:/downloadPack"],
          loginUser: {
            userId: "administrator",
            username: "admin",
            name: "超级管理员",
          },
        },
      };
    },
  },
  {
    url: `/mock/cardone-api/v1/system`,
    method: "post",
    timeout: 1000,
    response: () => {
      return {
        data: {
          navigationList: [
            {
              id: "/usercenter/",
              code: "/usercenter",
              href: "/usercenter",
              name: "用户中心",
              childs: [
                {
                  id: "/usercenter/department",
                  code: "/usercenter/department",
                  href: "/usercenter/department",
                  name: "部门管理",
                },
                {
                  id: "/usercenter/user",
                  code: "/usercenter/user",
                  href: "/usercenter/user",
                  name: "用户管理",
                },
              ],
            },
            {
              id: "/configuration",
              code: "/configuration",
              name: "配置",
              childs: [
                {
                  id: "/configuration/dictionaryType",
                  code: "/configuration/dictionaryType",
                  href: "/configuration/dictionaryType",
                  name: "字典类别",
                },
                {
                  id: "/configuration/dictionary",
                  code: "/configuration/dictionary",
                  href: "/configuration/dictionary",
                  name: "字典",
                },
              ],
            },
            {
              id: "/authority",
              code: "/authority",
              href: "/authority",
              name: "权限",
              childs: [
                {
                  id: "/authority/userGroup",
                  code: "/authority/userGroup",
                  href: "/authority/userGroup",
                  name: "用户组",
                },
                {
                  id: "/authority/role",
                  code: "/authority/role",
                  href: "/authority/role",
                  name: "角色",
                },
                {
                  id: "/authority/permission",
                  code: "/authority/permission",
                  href: "/authority/permission",
                  name: "许可",
                },
              ],
            },
            {
              id: "/log",
              code: "/log",
              href: "/log",
              name: "日志",
              childs: [
                {
                  id: "/log/operateLog",
                  code: "/log/operateLog",
                  href: "/log/operateLog",
                  name: "操作日志",
                },
              ],
            },
          ],
        },
      };
    },
  },
] as MockMethod[];
