const Index = () => {
  const location = useLocation();

  return <div>index：{location.pathname}</div>;
};

export default Index;
