import { axios1 } from "@cardone-web/core/axios";

export const logind = (config: any) => {
  return axios1({
    ...config,
    url: `/mock/cardone-api/v1/logind`,
    method: "post",
  });
};

export const system = (config: any) => {
  return axios1({
    ...config,
    url: `/mock/cardone-api/v1/system`,
    method: "post",
  });
};
