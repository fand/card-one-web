import { axios1 } from "@cardone-web/core/axios";

const logind = (config: any) => {
  return axios1({
    ...config,
    url: `/mock/cardone-api/v2/logind`,
    method: "post",
  });
};
