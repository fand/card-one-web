import axios from "axios";
import * as axiosWithExtra from "axios-with-extra";

export const { withErrorCustom, withErrorHelper, withExtraParams } = axiosWithExtra;

export const axios1 = axios.create({
  timeout: 900000,
  withCredentials: true,
});
