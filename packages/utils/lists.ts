export const getValueByKey = (key: any, items = [], { returnKey = true, keyName = "value", valueName = "label" } = {}) => {
  if (!key) {
    return "";
  }

  let item = items.find((it) => it[keyName] === key);

  if (item && item[valueName]) {
    return item[valueName];
  }

  if (returnKey) {
    return key;
  }

  return "";
};
