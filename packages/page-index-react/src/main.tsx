import React from 'react';
import ReactDOM from 'react-dom/client';
import { setupProdMockServer } from '~/mockProdServer';
import { ConfigProvider } from 'antd';
import zhCN from 'antd/es/locale/zh_CN';
import moment from 'moment';
import 'moment/locale/zh-cn';
import 'antd/dist/antd.less';
import './main.less';
import 'uno.css';
import Router from './router';
import hostMap from './hostMap';
import WujieReact from 'wujie-react';

WujieReact.preloadApp({ name: 'other', url: hostMap['other'], exec: true });
WujieReact.preloadApp({ name: 'system', url: hostMap['system'], exec: true });

setupProdMockServer();

moment.locale('zh-cn');

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ConfigProvider locale={zhCN}>
      <Router />
    </ConfigProvider>
  </React.StrictMode>
);
