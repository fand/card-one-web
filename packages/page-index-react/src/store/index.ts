export const securityAuthorizationAtom = atom('');

export const securityAuthoritiesAtom = atom([] as any[]);

export const securityLoginUserAtom = atom({} as any);
