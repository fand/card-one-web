import WujieReact from 'wujie-react';
import * as lifecycles from '../lifecycle';
import hostMap from '../hostMap';

const System = () => {
  const degrade = window.localStorage.getItem('degrade') === 'true';

  const navigation = useNavigate();

  return (
    <WujieReact
      width="100%"
      height="100%"
      name="system"
      url={hostMap['system']}
      alive={true}
      sync={true}
      props={{
        jump: (name: any) => {
          navigation(`/${name}`);
        },
      }}
      degrade={degrade}
      fiber={true}
      {...lifecycles}
    ></WujieReact>
  );
};

export default System;
