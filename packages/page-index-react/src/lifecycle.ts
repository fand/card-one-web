export const beforeLoad = (appWindow: any) => console.log(`${appWindow.__WUJIE.id} beforeLoad 生命周期`);

export const beforeMount = (appWindow: any) => console.log(`${appWindow.__WUJIE.id} beforeMount 生命周期`);

export const afterMount = (appWindow: any) => console.log(`${appWindow.__WUJIE.id} afterMount 生命周期`);

export const beforeUnmount = (appWindow: any) => console.log(`${appWindow.__WUJIE.id} beforeUnmount 生命周期`);

export const afterUnmount = (appWindow: any) => console.log(`${appWindow.__WUJIE.id} afterUnmount 生命周期`);

export const deactivated = (appWindow: any) => console.log(`${appWindow.__WUJIE.id} activated 生命周期`);

export const loadError = (url: string, e: any) => console.log(`${url} 加载失败`, e);
