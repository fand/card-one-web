import { MockMethod } from 'vite-plugin-mock';

import Mock from 'mockjs';

export default [
  {
    url: '/mock/cardone-api/v1/logind',
    method: 'post',
    timeout: 1000,
    response: () => {
      return {
        data: {
          authorization: 'test',
          authorities: ['ROLE_test', 'ROLE_administrator', 'navigation:view:/list', 'navigation:view:/downloadPack'],
          loginUser: {
            userId: 'administrator',
            username: 'admin',
            name: '超级管理员',
          },
        },
      };
    },
  },
  {
    url: `/mock/cardone-api/v1/index`,
    method: 'post',
    timeout: 1000,
    response: () => {
      return {
        data: {
          navigationList: [
            {
              id: '/app-1',
              code: '/app-1',
              href: '/app-1',
              name: '业务应用-1',
            },
            {
              id: '/app-2',
              code: '/app-2',
              href: '/app-2',
              name: '业务应用-2',
            },
            {
              id: '/app-3',
              code: '/app-3',
              href: '/app-3',
              name: '业务应用-3',
            },
            {
              id: '/app-4',
              code: '/app-4',
              href: '/app-4',
              name: '业务应用-4',
            },
            {
              id: '/system/',
              code: '/system/',
              href: '/system/',
              name: '系统管理',
            },
          ],
        },
      };
    },
  },
] as MockMethod[];
