import { MockMethod } from 'vite-plugin-mock';

import Mock from 'mockjs';

export default [
  {
    url: '/mock/cardone-api/v2/logind',
    method: 'post',
    timeout: 1000,
    response: () => {
      return {
        data: {
          authorities: ['ROLE_test', 'ROLE_administrator', 'navigation:view:/list', 'navigation:view:/downloadPack'],
          loginUser: {
            userId: 'administrator',
            username: 'admin',
            name: '超级管理员',
          },
        },
      };
    },
  },
  {
    url: `/mock/cardone-api/v2/scan-data`,
    method: 'post',
    timeout: 1000,
    response: () => {
      return {
        data: {
          userName: 'VIP 用户',
          navigationList: [
            {
              id: '/one-click-troubleshooting',
              code: '/one-click-troubleshooting',
              href: '/one-click-troubleshooting',
              name: '一键排查',
            },
            {
              id: '/troubleshooting-results',
              code: '/troubleshooting-results',
              name: '排查结果',
              childs: [
                {
                  id: '/tool-detection',
                  code: '/tool-detection',
                  href: '/tool-detection',
                  name: '风险应用',
                },
                {
                  id: '/trojans-detection',
                  code: '/trojans-detection',
                  href: '/trojans-detection',
                  name: '木马病毒',
                },
                {
                  id: '/emergency-investigation',
                  code: '/emergency-investigation',
                  href: '/emergency-investigation',
                  name: '系统风险',
                },
                {
                  id: '/log-analysis',
                  code: '/log-analysis',
                  href: '/log-analysis',
                  name: '异常日志',
                },
              ],
            },
            {
              id: '/knowledge-base',
              code: '/knowledge-base',
              href: '/knowledge-base',
              name: '知识库',
            },
          ],
        },
      };
    },
  },
] as MockMethod[];
