import { MockMethod } from 'vite-plugin-mock';

import Mock from 'mockjs';

export default [
  {
    url: '/mock/cardone-api/v1/scan-data/tool-detection/pageByIp',
    method: 'post',
    timeout: 1000,
    response: ({ body }) => {
      let data = {
        data: {
          number: body.pageNumber - 1,
          size: body.pageSize,
          totalElements: 100000,
          content: [] as any[],
        },
      };

      for (let i: number = 1; i <= body.pageSize; i++) {
        data.data.content.push({
          toolDetectionId: i,
          ip: Mock.Random.ip(),
          typeName: Mock.Random.cname(),
          name: Mock.Random.cname(),
          position: Mock.Random.region(),
          describe: Mock.Random.tld(),
          scanTime: Mock.Random.date('yyyy-MM-dd hh:mm:ss'),
        });
      }

      return data;
    },
  },
] as MockMethod[];
