import { MockMethod } from 'vite-plugin-mock';

import Mock from 'mockjs';

export default [
  {
    url: '/mock/cardone-api/v1/scan-data/log-analysis/pageByIp',
    method: 'post',
    timeout: 1000,
    response: ({ body }) => {
      let data = {
        data: {
          number: body.pageNumber - 1,
          size: body.pageSize,
          totalElements: 100000,
          content: [] as any[],
        },
      };

      for (let i: number = 1; i <= body.pageSize; i++) {
        data.data.content.push({
          logAnalysisId: i,
          ip: Mock.Random.ip(),
          testItem: Mock.Random.cname(),
          name: Mock.Random.cname(),
          exceptionFile: Mock.Random.region(),
          exceptionInfo: Mock.Random.tld(),
          exceptionTime: Mock.Random.date('yyyy-MM-dd hh:mm:ss'),
          treatmentScheme: Mock.Random.tld(),
          confirmation: Mock.Random.tld(),
          scanTime: Mock.Random.date('yyyy-MM-dd hh:mm:ss'),
        });
      }

      return data;
    },
  },
] as MockMethod[];
