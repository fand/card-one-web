import { MockMethod } from 'vite-plugin-mock';

import Mock from 'mockjs';

export default [
  {
    url: '/mock/cardone-api/v1/scan-data/findForCount',
    method: 'post',
    timeout: 1000,
    response: () => {
      let data = {
        data: {
          toolDetection: Mock.Random.integer(20, 50),
          trojansDetection: Mock.Random.integer(20, 50),
          emergencyInvestigation: Mock.Random.integer(20, 50),
          logAnalysis: Mock.Random.integer(20, 50),
        },
      };

      return data;
    },
  },
] as MockMethod[];
