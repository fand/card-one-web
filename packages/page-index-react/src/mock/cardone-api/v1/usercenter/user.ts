import { MockMethod } from 'vite-plugin-mock';

export default [
  {
    url: '/mock/cardone-api/v1/usercenter/user/m0002',
    method: 'post',
    timeout: 1000,
    response: () => {
      return {
        data: [],
      };
    },
  },
] as MockMethod[];
