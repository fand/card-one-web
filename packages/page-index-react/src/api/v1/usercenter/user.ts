import { axios1 } from '@cardone-web/core/axios';

const logout = (config: any) => {
  return axios1({
    ...config,
    url: '/apifox/cardone-api/v1/usercenter/user/logout',
    method: 'post',
  });
};

const login = (config: any) => {
  return axios1({
    ...config,
    url: '/apifox/cardone-api/v1/usercenter/user/login',
    method: 'post',
  });
};
