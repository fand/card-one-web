export default {
  "login": (process.env.NODE_ENV === "prod") ? "/login/" : "//localhost:8081/login/",
  "other": (process.env.NODE_ENV === "prod") ? "/other/" : "//localhost:8082/other/",
  "system": (process.env.NODE_ENV === "prod") ? "/system/" : "//localhost:8083/system/",
};

