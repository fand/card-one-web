import { createProdMockServer } from 'vite-plugin-mock/es/createProdMockServer';

const modules: Record<string, any> = import.meta.glob('./mock/**/*.ts', { eager: true });

export function setupProdMockServer() {
  const mockList: any[] = [];

  for (const key in modules) {
    mockList.push(...modules[key].default);
  }

  createProdMockServer(mockList);
}
