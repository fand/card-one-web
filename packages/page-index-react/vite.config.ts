import { defineConfig, loadEnv } from "vite";
import path from "path";
import legacy from "@vitejs/plugin-legacy";
import react from "@vitejs/plugin-react";
import AutoImport from "unplugin-auto-import/vite";
import Unocss from "unocss/vite";
import { presetUno, presetAttributify, presetIcons } from "unocss";

const resolve = (url) => path.resolve(url);

export default ({ mode }) => {
  const envConfig = loadEnv(mode, "./");

  return defineConfig({
    plugins: [
      react(),
      AutoImport({
        imports: ["ahooks", "react", "react-router-dom", "react-i18next", "jotai"],
        dts: "src/auto-import.d.ts",
        eslintrc: {
          enabled: true,
        },
        dirs: ["src/hooks", "src/store"],
      }),
      Unocss({
        presets: [presetUno(), presetAttributify(), presetIcons()],
      }),
      legacy({
        targets: ["Chrome 63"],
        additionalLegacyPolyfills: ["regenerator-runtime/runtime"],
        modernPolyfills: true,
      }),
    ],
    resolve: {
      alias: {
        "~/": `${resolve("src")}/`,
      },
    },
    css: {
      preprocessorOptions: {
        less: {
          // modifyVars: { "primary-color": "red" },
          javascriptEnabled: true,
        },
      },
    },
    server: {
      https: false,
      port: 8080,
      proxy: {
        "/cardone-api/": {
          target: "http://127.0.0.1:18888",
          changeOrigin: true,
        },
        "/apifox/cardone-api/": {
          target: "https://mock.apifox.cn",
          changeOrigin: true,
          secure: true,
          rewrite: (path) => path.replace(/^\/apifox/, "/m1/880791-0-default"),
        },
      },
    },
    build: {
      minify: "terser",
      target: "es2015",
      rollupOptions: {
        input: {
          index: resolve("index.html"),
        },
        output: {
          chunkFileNames: "index/static/js/chunk/[name].[hash].js",
          entryFileNames: "index/static/js/entry/[name].[hash].js",
        },
      },
      chunkSizeWarningLimit: 512, //大小警告的限制（以 kbs 为单位）
      outDir: "dist/" + envConfig.VITE_PROJECT_ENV + "/", //打包名称
      assetsDir: "index/static/asset/",
      terserOptions: {
        compress: {
          //打包去掉 console
          drop_console: true,
          //打包去掉 debugger
          drop_debugger: true,
        },
        keep_classnames: true,
      },
    },
  });
};
