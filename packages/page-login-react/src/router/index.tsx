import { HashRouter } from 'react-router-dom';
import { Suspense } from 'react';
import LoadingWaitFor from '~/components/LoadingWaitFor';

const LazyElement = ({ lazyComponent: LazyComponent }: any) => {
  return (
    <Suspense fallback={<LoadingWaitFor />}>
      <LazyComponent />
    </Suspense>
  );
};

export const routes = [
  {
    path: '/',
    element: <LazyElement lazyComponent={lazy(() => import('~/App'))} />,
  },
  {
    path: '*',
    element: <LazyElement lazyComponent={lazy(() => import('~/components/NotFound'))} />,
  },
];

const Routers = () => useRoutes(routes);

export default () => {
  return (
    <HashRouter>
      <Routers />
    </HashRouter>
  );
};
