import { MockMethod } from "vite-plugin-mock";

import Mock from "mockjs";

export default [
  {
    url: "/mock/cardone-api/v1/login",
    method: "post",
    timeout: 1000,
    response: () => {
      return {
        data: {
          authorization: "test",
        },
      };
    },
  },
] as MockMethod[];
