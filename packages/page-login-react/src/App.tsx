import { Button, message, Form, Input } from 'antd';
import { axios1, withErrorCustom, withErrorHelper } from '@cardone-web/core/axios';

function App() {
  //#region axios1 config
  useEffect(() => {
    withErrorCustom(axios1, response => {
      return response.status === 200 && !response.data?.errorCode;
    });

    withErrorHelper(axios1, (error: any) => {
      message.error({
        content: `${error.config?.errorMessage ?? ''}${error.response?.data?.error ?? error.message}`,
        duration: 5,
      });
    });
  }, []);
  //#endregion

  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      className="login-box"
    >
      <Form.Item label="用户名" name="username" rules={[{ required: true, message: '请输入用户名' }]}>
        <Input placeholder="请输入用户名" allowClear />
      </Form.Item>
      <Form.Item label="密码" name="password" rules={[{ required: true, message: '请输入密码' }]}>
        <Input.Password placeholder="请输入密码" visibilityToggle allowClear />
      </Form.Item>
      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          登录
        </Button>
      </Form.Item>
    </Form>
  );
}

export default App;
