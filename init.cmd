pnpm init

mkdir packages\core && cd packages\core && pnpm init && cd ..\..

mkdir packages\utils && cd packages\utils && pnpm init && cd ..\..

mkdir packages && cd packages 

pnpm create vite page-index --template recat-ts

pnpm create vite page-login --template recat-ts

pnpm create vite page-system --template recat-ts

cd ..

@REM ####################################################################################################

pnpm -w add -D typescript

pnpm -F @cardone-web/core add axios axios-with-extra

pnpm -F @cardone-web/page-index-react add wujie-react

pnpm -F @cardone-web/utils add lodash futil

pnpm -F @cardone-web/page-*-react add -D unplugin-auto-import unocss @unocss/preset-uno @unocss/preset-attributify @unocss/preset-icons @types/node less

pnpm -F @cardone-web/*-react add ahooks react react-router react-router-dom react-i18next jotai

@REM recoil react react-dom 
@REM pnpm add ahooks antd axios moment mockjs react-router-dom recoil
@REM pnpm add -D sass @vitejs/plugin-legacy vite-plugin-mock terser rollup
@REM pnpm i -w axios recoil react react-dom ahooks
@REM pnpm i -D -w typescript
@REM pnpm i -D typescript --filter @cardone-web/utils
@REM pnpm i @cardone-web/core --filter @cardone-web/page-index
@REM pnpm i @cardone-web/store --filter @cardone-web/page-index
@REM pnpm i @cardone-web/utils --filter @cardone-web/core
@REM pnpm i antd @ant-design/icons ahooks moment mockjs react-router-dom recoil --filter @cardone-web/page-index
@REM pnpm i -D sass @vitejs/plugin-legacy vite-plugin-mock terser rollup --filter @cardone-web/page-index
@REM pnpm -F @cardone-web/page-index dev

@REM ####################################################################################################


for /R  %s in (.,*) do  rd /q /s %s\node_modules

for /R  %s in (.,*) do  rd /q /s %s\dist

pnpm i 







